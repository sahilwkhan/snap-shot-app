import React, { Component } from 'react'

class Search extends Component {

  state = {
    searchText: ''
  };


  onChange = (changed) => {
    this.setState({searchText : changed.target.value})  
  }

  onSubmit = (clicked) => {
    clicked.preventDefault();
    this.props.getSearchText(this.state.searchText);
    this.setState({ searchText: '' }); 
  }

  render() {
      return (
        <div style={ searchBarStyle} >
            <h1  style={ searchBarStyle} >Search your image here </h1>
            <form onSubmit={this.onSubmit} style={inputTextStyle}>
              <input
                style= {inputTextStyle}
                type='text'
                name='title'
                placeholder='Search Images'
                value={this.state.searchText}
                onChange={this.onChange}
              />
              <input
                type='submit'
                value='Submit'
                className='btn'
                style={inputBarStyle}
              />
            </form>
        </div>
    )
  }
}

const searchBarStyle = {
    padding: '1rem',
    textAlign: 'center',
    justifyContent:'start'

}
const inputTextStyle = {
  padding: '0.2rem',
  paddingLeft: '0.5vw',
  paddingRight: '4vw'
}
const inputBarStyle = {
  margin: '1rem',
  padding: '0.2rem 1rem',
  borderRadius: '0.5rem'
}

export default Search;