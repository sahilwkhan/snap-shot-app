import { Component } from 'react';
import './App.css';


import Header from './components/layout/header';
import Search from './components/search/Search';

const apiKey = '8e009189ebc5347b4f14f3fffce91fa3';
const perPage = 20; 
const page = 2;


function fetchForSearch(tag) {
  return new Promise((resolve, reject) => {
    fetch(`https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${apiKey}&tags=${tag}&per_page=${perPage}&page=${page}&format=json&nojsoncallback=1&safe_search=1`)
      .then((response) => {
        return response.json();
      })
      .then((imgData) => {
        const photoList = imgData.photos.photo;
        const imgList = photoList.map((photo) => {
        return [photo.id, `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`, photo.title];
      });
      resolve(imgList);
    })
    .catch((errorWhileFetching) => {
      console.log(`Unable to fetch photos for ${tag}`);
      reject(errorWhileFetching);
    });
  })
}

class App extends Component {
  state = {
    images: [],
    isLoading: false
  }

  componentDidMount() {
    this.setState({ isLoading: true }); 
    fetchForSearch("Mountain")
      .then((imgList) => {
        this.setState({ images: imgList, isLoading: false,searchText : 'Mountain' }); 
      })
      .catch((error) => {
        console.log(error);
        this.setState({ isLoading: false });
      });
  }

  getSearchButton = (buttonText) => {
    this.setState({ isLoading: true });
    fetchForSearch(buttonText)
      .then((imgList) => {
        this.setState({ images: imgList, isLoading: false, searchText:buttonText }); 
      })
      .catch((error) => {
        console.log(error);
        this.setState({ isLoading: false });
      });
  }

  getSearchText = (searchText) => {
    this.setState({ isLoading: true });
    fetchForSearch(searchText)
      .then((imgList) => {
        this.setState({ images: imgList });
        this.setState({ images: imgList, isLoading: false, searchText:searchText}); 
      })
      .catch((error) => {
        console.log(error);
        this.setState({ isLoading: false });
      });
  };

  render() {  
    const { images, isLoading,searchText} = this.state; 


    return (
      <div className="App">
        <Header />
        <Search getSearchText={this.getSearchText} />
        
        <div style={searchButtonStyle}>
          <button className='buttonStyle' onClick={() => this.getSearchButton('Nature')}>Nature</button>
          <button className='buttonStyle' onClick={() => this.getSearchButton('City')}>City</button>
          <button className='buttonStyle' onClick={() => this.getSearchButton('Vehicles')}>Vehicles</button>
          <button className='buttonStyle' onClick={() => this.getSearchButton('Food')}>Food</button>
        </div>
      
        <div>
          <div>
              <h1>Search Results for: {searchText}</h1>
          </div>
          
          <div className="image-Container">

            {
              isLoading ? (
                <div>
                  <h1>Your Image are loading</h1>
                  <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
              ) : (
                
                (images.length > 0) ? (
                  images.map((imageUrl) => {
                    return <img style={frontImg} key={imageUrl[0]} src={imageUrl[1]} alt={`Image ${imageUrl[0]}`} />
                  })
                ) : (
                  <h1>Error: Unable to fetch Images</h1>
                )
              )
            }
          </div>
          
        </div>
        
      </div>
    );
  }
}

const searchButtonStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
  gap:'2rem',
}


const frontImg = {
  width: '12rem',
  height: '12rem',
  marginBottom: '2rem'
}

export default App;
